<?php
/**
 * @file
 * automatic_product_display.features.inc
 */

/**
 * Implementation of hook_node_info().
 */
function automatic_product_display_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('A product that\'s sold in the store.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
