<?php
/**
 * @file
 * automatic_product_display.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function automatic_product_display_default_rules_configuration() {
  $items = array();
  $items['rules_create_product_display_for_new_commerce_product'] = entity_import('rules_config', '{ "rules_create_product_display_for_new_commerce_product" : {
      "LABEL" : "Create Product Display for new Commerce Product",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "commerce" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_product_insert" ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "product",
              "param_title" : [ "commerce-product:title" ],
              "param_author" : [ "commerce-product:creator" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-product:0" ],
            "value" : [ "commerce-product" ]
          }
        }
      ]
    }
  }');
  $items['rules_delete_product_display_when_commerce_product_is_deleted'] = entity_import('rules_config', '{ "rules_delete_product_display_when_commerce_product_is_deleted" : {
      "LABEL" : "Delete Product Display when Commerce Product is deleted",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "commerce" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_product_delete" ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_product",
              "value" : [ "commerce-product:product-id" ]
            },
            "PROVIDE" : { "entity_fetched" : { "product_display" : "Fetched entity" } }
          }
        },
        { "entity_delete" : { "data" : [ "product-display:0" ] } }
      ]
    }
  }');
  $items['rules_update_product_display_for_existing_commerce_product'] = entity_import('rules_config', '{ "rules_update_product_display_for_existing_commerce_product" : {
      "LABEL" : "Update Product Display for existing Commerce Product",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "commerce" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_product_update" ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_product",
              "value" : [ "commerce-product:product-id" ]
            },
            "PROVIDE" : { "entity_fetched" : { "product_display" : "Fetched entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "product-display:0:title" ],
            "value" : [ "commerce-product:title" ]
          }
        },
        { "data_set" : {
            "data" : [ "product-display:0:author" ],
            "value" : [ "commerce-product:creator" ]
          }
        },
        { "entity_save" : { "data" : [ "product-display:0" ] } }
      ]
    }
  }');
  return $items;
}
